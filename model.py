import random

#set of possible moves a player can make (R)ock, (P)aper, (S)cissors
moves = ['R','P','S']

#generate a random environment of the form [('r','s'),('r','p').....
generate_random_environment = lambda max,r: [(moves[r.randrange(0,3)],moves[r.randrange(0,3)]) for i in range(0,max)]

#our set of Functional nodes has a state because the payoff matrix is a user defined variable
#  the payoff matrix is related to the funcional set, because the functional set evaluates it to determine winner,loser,draw
class Functional:
  def __init__(self,r=random,payoff=None):
    self.r = r #random object 
    if payoff == None or payoff == 'default': #set payoff_table to user defined payoff or default specified in assignment series 2
      self.payoff_table = {'R':{'R':0,'P':1,'S':-1},'P':{'R':-1,'P':0,'S':1},'S':{'R':1,'P':-1,'S':0}}
    else:
      self.payoff_table = {'R':{'R':payoff[0],'P':payoff[1],'S':payoff[2]},'P':{'R':payoff[3],'P':payoff[4],'S':payoff[5]},'S':{'R':payoff[6],'P':payoff[7],'S':payoff[8]}}
    #define functional set, map a string to a function call 
    self.functional_set={'winner':self.winner,'loser':self.loser,'draw':self.draw}
  #rps logic operators
  #returns player's move if draw (is the same as opponent), otherwise player's move if player is the winner
  #  returns opponent if player loses 
  def winner(self,plr,opp):
    if self.payoff_table[opp][plr] >= 0:
      return plr
    else:
      return opp

  #returns player's move if draw (is the same as opponent), otherwise opponent's move if player is the winner
  #  returns player if player loses
  def loser(self,plr,opp):
    if self.payoff_table[opp][plr] <= 0:
      return plr
    else:
      return opp
    
  #returns player's move if draw (is the same as opponent), otherwise returns the move not used by either player or opponent
  def draw(self,plr,opp):
    if plr == opp:
      return plr
    else:
      return list(set(moves)-set([plr,opp])).pop()
   
  #returns a randomly selected function to set as a node value
  def rand_func(self):
    return self.functional_set.keys()[self.r.randrange(0,10*len(self.functional_set.keys()))%3]

class Terminal:
  def __init__(self,k_agent_mem,environment,r=random):
    self.r=r #random object identifier
    #create terminal keys p0...pk-1 o0...ok-1
    self.terminal_keys = ['p'+str(i) for i in range(0,k_agent_mem)] + ['o'+str(i) for i in range(0,k_agent_mem)]
    #decode terminal will let us take an input sequence pk/ok and map to to a tuple in the RPS list 
    self.decode_terminal = lambda x,environment: environment[int(x[1])][(0,1)[x[0]=='o']]
    #create a terminal set where keys are mapped to moves in the environment, exampple p0:'r' o5:'s' 
    self.terminal_set = {key:value for (key,value) in [(terminal,self.decode_terminal(terminal,environment)) for terminal in self.terminal_keys] }
  def rand_term(self): #return a randomly selected terminal value
    return self.terminal_keys[self.r.randrange(0,10*len(self.terminal_keys))%len(self.terminal_keys)]

class Node:
  def __init__(self,parent,value,level):
    self.parent=parent #reference to node's parent
    self.lchild=None #reference to node's left child
    self.rchild=None #reference to node's right child
    self.value=value #nodes value, is a member of (functional U terminal) keys
    self.level=level #level of the node in the tree, 0->k-1
  @classmethod #basically a copy constructor
  def fromNode(cls,node):
    return cls(node.parent,node.value,node.level)


#term is an instance of the terminal set
#funcs is an instance of the functional set 
#dmax: max depth of the tree
#coef: coefficient determining the probability of terminating branch growth before it reaches d_max_depth
#  is a value between 0 and 10. higher value means higher chance of terminating growth
#r: pointer to an instance of random
class StrategyTree:
  def __init__(self,terms,funcs,dmax,coef=0,r=random,target=None):
    self.coef = coef 
    self.r = r
    self.dmax = dmax
    self.terms = terms
    self.funcs = funcs
    self.fitness = 0.0
    self.current_depth = 0
    self.nodelist = list() #list of pointers to each node

    if target == None:
      if dmax == 0:
        self.root = Node(None,self.terms.rand_term(),0) #if max depth is zero, the root node is a terminal
      else:
        self.root = Node(None,funcs.rand_func(),0)  #otherwise, we recursive build the tree 
        self._BuildTree(self.root,1,self.coef)
    else:
      self.root = Node.fromNode(target.root)
      self._CopyTree(target.root,self.root)

    self.UpdateNodes()

  #used to deepcopy one tree from another
  def _CopyTree(self,target,copy):
    if target.lchild != None and target.rchild != None:
      copy.lchild = Node.fromNode(target.lchild)
      copy.rchild = Node.fromNode(target.rchild)
      copy.lchild.parent = copy
      copy.rchild.parent = copy
      self._CopyTree(target.lchild,copy.lchild)
      self._CopyTree(target.rchild,copy.rchild)

  #_BuildTree builds a binary tree of height dmax, or up to dmax if coef > 0, with randomly selected
  #  functional and terminal nodes
  #node: node to build tree from
  #current: current level of the recursion
  def _BuildTree(self,node,current,coef):
    if current < self.dmax-1: #if we haven't reached the max height
      if coef > 0: #if coef > 0, there is a chance to stop growth before dmax reached
        if self.r.randrange(0,10) in range(0,coef):
          node.lchild = Node(node,self.terms.rand_term(),current)
          node.rchild = Node(node,self.terms.rand_term(),current)
          return None
      #add a new functional node to left and right, continue growing tree
      node.lchild = Node(node,self.funcs.rand_func(),current)
      node.rchild = Node(node,self.funcs.rand_func(),current)
      self._BuildTree(node.lchild,current+1,coef)
      self._BuildTree(node.rchild,current+1,coef)
    else: #if we've reached max depth, add terminal nodes and stop growth
      node.lchild = Node(node,self.terms.rand_term(),current)
      node.rchild = Node(node,self.terms.rand_term(),current)

  #get information about tree nodes in a string and update the level each node is at
  def _Nodes(self,node=None):
    if node == None:
      node = self.root
      self.UpdateNodes()
    ostr='('+node.value+' '+str(node.level)+') '
    if node.lchild != None and node.rchild != None:
      ostr+=self._Nodes(node.lchild)
      ostr+=self._Nodes(node.rchild)
    return ostr
  
  #lets us call print on a tree
  def __str__(self):
    return self._Nodes()

  #make sure each node's level is correct, and our nodelist is correct
  def UpdateNodes(self,node=None, current = 0):
    if node == None:
      node = self.root
      self.nodelist = list()
    if current > self.current_depth:
      self.current_depth = current
    node.level=current #make sure the tree's level is reported correctly (it would only change if the tree was modified)
    self.nodelist.append(node)
    if node.lchild != None and node.rchild != None:
      self.UpdateNodes(node.lchild,current+1)
      self.UpdateNodes(node.rchild,current+1)

  #ParseTree returns the evaluation of the tree
  #environment: list of tuples containing RPS pairs that describe the agent states
  def ParseTree(self,environment,node=None):
    #assert((node.lchild == None and node.rchild == None) or (node.lchild != None and node.rchild != None))
    if node == None:
      node = self.root
    if node.lchild != None and node.rchild != None: #if the children are not none, go deeper 
      return self.funcs.functional_set[node.value](self.ParseTree(environment,node.lchild),self.ParseTree(environment,node.rchild))
    else: #otherwise, evaluate the node because it will be terminal
      #assert(node.value in self.terms.terminal_keys)
      return self.terms.decode_terminal(node.value,environment)
  
  #make sure the tree doesnt have any weird functionals or terminals
  def ValidateTree(self,node=None):
    if node == None:
      node = self.root
    if node.lchild != None and node.rchild != None:
      assert(node.value in self.funcs.functional_set.keys())
      self.ValidateTree(node.lchild)
      self.ValidateTree(node.rchild)
    else:
      assert(node.value in self.terms.terminal_set.keys())

#testing
if __name__==  "__main__": #can use this to test the tree
  k = 4 #agent mem
  d = 5 #tree depth
  c = 0 #chance to prohibit growth
  env = generate_random_environment(k,random)
  term = Terminal(k,env)
  func = Functional()
  tree = StrategyTree(term,func,d,c,random)
  print 'Outcome 1:', tree.ParseTree(env), env
  env.pop()
  env.insert(0,('R','R'))
  print 'Tree:',tree,'\n'
  print 'Environment:',env,'\n'
  print 'Outcome 2:', tree.ParseTree(env),env
  #print test
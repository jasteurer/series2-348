#use this file to find data for random search 
import sys
import os.path
from os.path import join,exists
from os import makedirs
import random
import time
from model import generate_random_environment, Functional, Terminal
from ea import GPEA, FitnessCalc

print 'Joseph A Steurer - CS348 - Assignment 2b randsearch'
print 'config file format:'
print 'l_seq_len, k_agt_mem, d_max_depth, seed/timer,evals,runs'
print 'log file name'
print 'sol file name'
print 'payoff,0,1,-1,-1,0,1,1,-1,0/default payoff matrix'
print 'strategy1/strategy2'
print 'mu,lambda,parsimony_coef'
print 'parent selection: over/fps'
print 'survivor selection: trunc/ksize'
print
print 'Output is stored in a directory named after the config file.'

#read config file
if len(sys.argv) > 1:
  cfile = sys.argv[1] #specify as first argument of command line
else:
  cfile = 'randsearch.cfg' #select a default configuration

cfg = open(cfile,'r') #open the config file

#cfg.readline() #discard first line, its a comment. 
tmp = cfg.readline().strip().split(',') #read the next line into a temporary variable

l_seq_len = int(tmp[0]) #the first element of tmp is the sequence length
k_agt_mem = int(tmp[1]) #the second is the agent memory length
d_max_depth = int(tmp[2]) #this is the maximum depth of the tree
if tmp[3] == 't': #random seed is timer
  r_seed = int(round(time.time() * 1000)) #set the seed based on a time
else: #random seed it init from file
  r_seed = int(tmp[3]) #set the seed based on a value in the file
evals = int(tmp[4]) #get the number of fitness evaluations to perform
runs = int(tmp[5])

#here, we dont read in logfile and solfile directories. 
#  the output directory is always dependent on the name of the config file
logfile = cfg.readline() #get the name of the logfile
solfile = cfg.readline() #get the name of the solution file

#read in user defined payoff matrix into a temporary variable
tmp = cfg.readline().strip().split(',')

if tmp[0] == 'payoff': #if the user specifies 'payoff' we will read in a payoff matrix
  payoff = list()
  for i in tmp[1:]:
    payoff.append(int(i)) #in payoff, we will store a 9 element list of matrix values, which takes the form of
    #row1: c1,c2,c3, row2.....
else:
  payoff = 'default'

opponent_strat_type = cfg.readline().strip() #next,read in the opponent strategy type 

tmp = cfg.readline().strip().split(',')
popsize = int(tmp[0])
offsize = int(tmp[1])
parsimony = float(tmp[2])
p_r = int(tmp[3])
p_m = int(tmp[4])

psel = cfg.readline().strip()
ssel = cfg.readline().strip()

cfg.close() #we're done with the config file

#init random object with seed
rptr = random.Random() #initialize a random object called rptr
rptr.seed(r_seed) #set the random seed. all important random calls use this seed


#make the directory for the log and solution files, ensure that one doesnt exist
if not os.path.exists(cfile.split('.')[0].upper()):
    os.makedirs(cfile.split('.')[0].upper())
else:
  raise Exception('solution and log directory exists! please do not overwrite')

#open log file and write logfile header
log = open(join(cfile.split('.')[0].upper(),logfile.strip()),'w')
log.write('Agent Memory: '+str(k_agt_mem)+ ' Maximum Tree Depth: '+ str(d_max_depth) + ' Seed: ' + str(r_seed)+'\n')
log.write('Mu: '+str(popsize)+' Lambda: '+str(offsize)+' Parent Sel: '+str(psel)+' Survivor Sel: '+str(ssel)+' p_m: '+str(p_m)+' p_r: '+str(p_r)+' \n')

#initially, bgf is zero
best_global_fitness = 0
#do the experiment

no_fevals = (evals-popsize)/offsize

environment = generate_random_environment(k_agt_mem,rptr)
fc = FitnessCalc(rptr, l_seq_len, k_agt_mem, 1.0, opponent_strat_type)
terms = Terminal(k_agt_mem,environment,rptr)
funcs = Functional(rptr,payoff) 


stat_data = dict()

#for statanal, we need avg pop fit for 30 runs
#for graph, we need avg evals avged over 30 runs
#fitness is average payoff


for r in range(0,runs):
  print 'Run: ',r+1
  log.write('Run: '+str(r+1)+'\n')
  gp = GPEA(rptr, evals, 0, terms, funcs, d_max_depth, fc)
  for peon in gp.population:
    gp.fitcalc.eval(peon)

  avg_pop_fit = sum(peon.fitness for peon in gp.population) / popsize
  max_pop_fit = max(peon.fitness for peon in gp.population)
  
  log.write(str(runs)+'\t'+str(avg_pop_fit)+'\t'+str(max_pop_fit)+'\n')
  if max_pop_fit > best_global_fitness:
    for peon in gp.population:
      if peon.fitness == max_pop_fit:
        best_global_fitness=max_pop_fit 
        sol = open(join(cfile.split('.')[0].upper(),solfile.strip()),'w')
        sol.write(peon.__str__())
        sol.close()
        break

  stat_data[r]=avg_pop_fit
  log.write('\n')
  print '\tAverage pop fitness: ',avg_pop_fit,' Max pop fitness',max_pop_fit
log.close() #close the log file


exceldata = open(join(cfile.split('.')[0].upper(),logfile.strip()+'.txt'),'w')
exceldata.write(cfile+'\n')
for i in stat_data.keys():
  exceldata.write(str(i)+'\t'+str(stat_data[i])+'\n')
exceldata.write('\n')

figure1=figure;
hold on;
Evaluations = [2,3,5,10,15,1000];
Fitness_OppStrat1 = [.01,.45,.55,.64,1.0,1.0];
plot(Evaluations,Fitness_OppStrat1,'Color','r','LineWidth',1.15,'Marker','s');
axis([0 1000 0 2]);
set(gca,'XScale','log');
set(title('Fintess vs Evals, Opponent Strategy 1'),'FontSize',14);
set(xlabel('Evaluations'),'FontSize',12);
set(ylabel('Fitness'),'FontSize',12);
legend('Location','BestOutside','Strategy 1 BGF');
saveas(figure1,'~/Desktop/Solution1.jpg');
clear;
close;


figure1=figure;
hold on;
Evaluations = [0,46,621,1000]
Fitness_OppStrat2 = [.3,.5,.6,.6]
plot(Evaluations,Fitness_OppStrat2,'Color','b','LineWidth',1.15,'Marker','s')
axis([0 1000 0 2]);
set(gca,'XScale','log');
set(title('Fintess vs Evals, Opponent Strategy 2'),'FontSize',14);
set(xlabel('Evaluations'),'FontSize',12);
set(ylabel('Fitness'),'FontSize',12);
legend('Location','BestOutside','Strategy 2 BGF');
saveas(figure1,'~/Desktop/Solution2.jpg');
clear;
close;
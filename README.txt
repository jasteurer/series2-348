In reports/2b, you'll find the spreadsheet from which I derived the report stats and plots.
I've omitted termination criteria because I haven't ever used it and it clutters up my assignment.py scripts and config files...
randsearch.py generates results for random search
The results from all of my experiments, and the additional config files, are in the CONFIGS directory
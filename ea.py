from model import StrategyTree, Functional, Terminal, generate_random_environment, moves
#rptr: pointer to random object
#psize: mu
#osize: lambda
#tset: set of terminals
#fset: set of functionals
#dmax: maximum height of tree
#fc: reference to a fitness calculator object for doing a fitness eval of an individual 
class GPEA: 
  def __init__(self, rptr, psize, osize, tset, fset, dmax, fc):
    self.rptr = rptr
    self.population = list()
    self.offspring = list()
    self.tset = tset
    self.fset = fset
    self.psize = psize
    self.osize = osize
    self.dmax = dmax
    self.fitcalc = fc

    #initalize population with half and half ramped strategy
    for i in range(0, self.psize):
      if 0 == self.rptr.randrange(0,2): #tree of dmax
        self.population.append(StrategyTree(tset,fset,dmax,0,self.rptr))
      else: #tree < dmax
        self.population.append(StrategyTree(tset,fset,dmax,self.rptr.randrange(0,10),self.rptr))
  
    #calculate fitness of initial population
    for peon in self.population:
      self.fitcalc.eval(peon)

  #overselection for parents
  #x: value between 0.1 and 1 determining how we split our population for selection
  def POverSelect(self,x):
    if x < 0.01 or x > 1.0:
      raise Exception('X must be between .01 and 1')
    self.offspring = list()
    pop_list = list()
    for i in range(0, self.psize): #get a list of indicies of parents and their corresponding fitness
      pop_list.append((i,self.population[i].fitness))
    pop_list = sorted(pop_list,key = lambda tup:tup[1],reverse=True)
    
    #divide population into groups
    group1 = pop_list[:int(x*len(pop_list))]
    group2 = pop_list[int(x*len(pop_list)):]

    #select 80% from group1 and 20% from group 2, make sure we're making copies into the offspring
    for i in range(0, int(self.osize*.8)):
      self.offspring.append(StrategyTree(self.tset,self.fset,self.dmax,0,self.rptr,self.population[group1[self.rptr.randrange(0,len(group1))][0]]))
    for i in range(0,self.osize-int(self.osize*.8)):
      self.offspring.append(StrategyTree(self.tset,self.fset,self.dmax,0,self.rptr,self.population[group2[self.rptr.randrange(0,len(group2))][0]]))

    #assert(len(self.offspring) == self.osize)
    #for ptr in self.population:
      #assert(sum(1 for obj in self.offspring if obj == ptr) < 2)

  #fitness proportional selection for parents
  def PFPS(self):
    self.offspring = list() 
    #we need to convert population fitnesses from decimals and negative numbers to positive whole numbers
    intadjust = self.fitcalc.l_seq_len - (2*self.fitcalc.k_agt_mem) 
    lowadj = abs(min(int(parent.fitness*intadjust) for parent in self.population))
    #find sigma f
    sigmaf = sum(int(parent.fitness*intadjust)+lowadj for parent in self.population)

    #if the whole population is fitness zero, equal chance of selection
    if sigmaf == 0.0: #if the sum of the fitnesses of the entire population is zero, randomly select parents
      for i in range(0, self.osize):
        randParentIndex = self.rptr.randrange(0, self.psize)
        self.offspring.append(StrategyTree(self.tset,self.fset,self.dmax,0,self.rptr,self.population[randParentIndex]))
    else: # f/sigmaf chance to be a parent
      for i in range(0, self.osize): #consider adding logic to kill this while after while
        randParentIndex = self.rptr.randrange(0, self.psize)
        while self.rptr.randrange(0,sigmaf) not in range(0,int(self.population[randParentIndex].fitness*intadjust)+lowadj):
          randParentIndex = self.rptr.randrange(0, self.psize)
        self.offspring.append(StrategyTree(self.tset,self.fset,self.dmax,0,self.rptr,self.population[randParentIndex]))

  #for our mutation and recombination, we work out of the offspring pool containing copies of parents
  #this way, we're not overwriting parents 

  #recombine parents to make offspring
  #p_recombine: chance between 1 and 10 to allow recombination 
  def Recombine(self, p_recombine):
    if p_recombine > 0:
      #choose two parents to mate
      for mate_pair in zip(self.offspring[:len(self.offspring)/2], self.offspring[len(self.offspring)/2:]):
        if self.rptr.randrange(0,10) in range(0,p_recombine):
          
          crossover_node1 = self.rptr.randrange(1,len(mate_pair[0].nodelist))
          crossover_node2 = self.rptr.randrange(1,len(mate_pair[1].nodelist))

          parent_node1  = mate_pair[0].nodelist[crossover_node1].parent
          parent_node2  = mate_pair[1].nodelist[crossover_node2].parent

          if mate_pair[0].nodelist[crossover_node1].parent.lchild == mate_pair[0].nodelist[crossover_node1]:
            tmp = parent_node1.lchild 
            parent_node1.lchild = mate_pair[1].nodelist[crossover_node2]
            parent_node1.lchild.parent = parent_node1
          elif mate_pair[0].nodelist[crossover_node1].parent.rchild == mate_pair[0].nodelist[crossover_node1]:
            tmp = parent_node1.rchild 
            parent_node1.rchild = mate_pair[1].nodelist[crossover_node2]
            parent_node1.rchild.parent = parent_node1
          else:
            raise Exception('Something went horribly wrong in recombination')

          if mate_pair[1].nodelist[crossover_node2].parent.lchild == mate_pair[1].nodelist[crossover_node2]:
            parent_node2.lchild = tmp
            tmp.parent = parent_node2
          elif mate_pair[1].nodelist[crossover_node2].parent.rchild == mate_pair[1].nodelist[crossover_node2]:
            parent_node2.rchild = tmp
            tmp.parent = parent_node2
          else:
            raise Exception('Something went horribly wrong in recombination')

          mate_pair[0].UpdateNodes()
          mate_pair[1].UpdateNodes()

          #mate_pair[0].ValidateTree()
          #mate_pair[1].ValidateTree()

  #p_mutate: chance between 1 and 10 to allow mutation
  def Mutate(self, p_mutate):
    if p_mutate > 0:
      for child in self.offspring:
        if self.rptr.randrange(0,10) in range(0,p_mutate):
          mutate_node_target = self.rptr.randrange(1,len(child.nodelist)) 
          subtree = StrategyTree(self.tset,self.fset,self.rptr.randrange(1,self.dmax),(0,self.rptr.randrange(0,10))[self.rptr.randrange(0,2)],self.rptr) 
          if child.nodelist[mutate_node_target].parent.lchild == child.nodelist[mutate_node_target]:
            child.nodelist[mutate_node_target].parent.lchild = subtree.root
            subtree.root.parent = child.nodelist[mutate_node_target].parent.lchild
          elif child.nodelist[mutate_node_target].parent.rchild == child.nodelist[mutate_node_target]:
            child.nodelist[mutate_node_target].parent.rchild = subtree.root
            subtree.root.parent = child.nodelist[mutate_node_target].parent.rchild
          else:
            raise Exception('Something went horribly wrong during mutation.')
          child.UpdateNodes()

  #put offspring back into the population with the truncation method
  def STruncate(self):
    for peon in self.offspring:
      self.fitcalc.eval(peon)
    fitness_index_list = list()
    for index,peon in enumerate(self.population):
      fitness_index_list.append((index,peon.fitness))
    fitness_index_list=sorted(fitness_index_list,key=lambda tup:tup[1])
    for i in range(0, self.osize):
      if i == (self.psize - 1):
        break
      self.population[fitness_index_list[i][0]] = self.offspring[i]
    self.offspring = list()

  #put offspring back into the population with the k tournament method
  def SKtourn(self,k):
    for peon in self.offspring:
      self.fitcalc.eval(peon)
    fitness_index_list = list()
    for index,peon in enumerate(self.population):
      fitness_index_list.append((index,peon.fitness))
    fitness_index_list=sorted(fitness_index_list,key=lambda tup:tup[1])
    
    selected_for_execution = list()
    for i in range(0, self.osize):
      if i == (self.psize - 1):
        break
      k_list = list()
      for j in range(0,k):
        target = fitness_index_list[self.rptr.randrange(0,len(fitness_index_list))]
        while target in selected_for_execution:
          target = fitness_index_list[self.rptr.randrange(0,len(fitness_index_list))]
        k_list.append(target)
      self.population[sorted(k_list,key = lambda tup:tup[1])[0][0]] = self.offspring[i]
      selected_for_execution.append(sorted(k_list,key = lambda tup:tup[1])[0])
    self.offspring = list()
    
#this class lets us select an opponent strategy and perform l turns against the selected strategy for a
#  GP candidate solution, then updates the fitness of the candidate solution based on the outcome of this test
class FitnessCalc:
  def __init__(self, rptr, l_seq_len, k_agt_mem, pars=1.0, strat2file=None):
    if l_seq_len < 3 * k_agt_mem:
      raise Exception('l_seq_len must be >= 3*k_agt_mem')
    if pars < 0.01 or pars > 1.0:
      raise Exception('Parsimony pressure must be between 0.01 and 1.0')

    self.l_seq_len = l_seq_len
    self.k_agt_mem = k_agt_mem
    self.rptr = rptr
    self.env = generate_random_environment(k_agt_mem,rptr)
    self.oppstrat = None
    self.parsimony_coef = pars

    if strat2file == 'strategy1' or strat2file == None:
      self.oppstrat = moves[self.rptr.randrange(0,len(moves))]

    else:
      oppfile = open(strat2file,'r')
      #we're going to read in the entire file to a dict, that way we only do disk IO once
      self.oppstrat = dict()
      assert(int(oppfile.readline().strip()) == self.k_agt_mem) #the agent memory specified in the opponent strategy must be the same as the config file
      for line in oppfile:
        tmp = line.strip()
        #assert(tmp[:len(tmp)-2] not in opp_strat.keys()) #every line in the file is unique
        self.oppstrat[tmp[:len(tmp)-2]]=tmp[-1]
    
  #index into an opponent strategy based on the state of the current environment
  def _env_to_str(self,env):
    tmp = ''
    for tup in env:
      tmp+=str(tup[0])+','+str(tup[1])+','
    return tmp[:len(tmp)-1]

  def eval(self,player):
    self.env=generate_random_environment(self.k_agt_mem,self.rptr) #we get awful results if we dont reset the enviroment for each evaluation
    player.fitness = 0.0
    for i in range(0, self.l_seq_len):
      opp=None
      plr = player.ParseTree(self.env)
      if isinstance(self.oppstrat, dict):
        opp = self.oppstrat[self._env_to_str(self.env)]
        if i > (2 * self.k_agt_mem)-1: 
          player.fitness+=player.funcs.payoff_table[opp][plr]
      else:
        opp=self.oppstrat
        if i > (2 * self.k_agt_mem)-1:
          player.fitness+=player.funcs.payoff_table[opp][plr]
        self.oppstrat=player.funcs.winner(plr,opp)
      self.env.insert(0,(plr,opp)) #add the newest tuple to the front
      self.env.pop() #take the last (oldest) tuple off the back of the list of RPS tuples
        #assert(plr in moves and opp in moves)
    if self.parsimony_coef < 1.0:
      player.fitness /= float(self.l_seq_len)-float((2*self.k_agt_mem)) 
      if player.current_depth > player.dmax:
        player.fitness *= self.parsimony_coef
    else:
      player.fitness /= float(self.l_seq_len)-float((2*self.k_agt_mem))
    

if __name__==  "__main__": #can use this to test the tree
  import random
  k = 4 #agent mem
  d = 3 #tree depth
  c = 0 #chance to prohibit growth
  env = generate_random_environment(k,random)
  term = Terminal(k,env)
  func = Functional()
  pars = .5
  gp = GPEA(random, 10, 10, term, func, d,FitnessCalc(random, 12,k,pars,"OPPONENTS/opponent1.csv"))
  for i in range(0,1):
    gp.PFPS()
    #gp.POverSelect(.5)
    gp.Recombine(10)
    gp.SKtourn(10)
  for i,p in enumerate(gp.population):
    print i,p,p.fitness
  #from itertools import chain 
  #print list(chain.from_iterable([f.values() for f in func.payoff_table.values()]))


